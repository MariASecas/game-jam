﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Personaje1 : MonoBehaviour
{
    Rigidbody2D rigidCharacter;
    SpriteRenderer characterSprite;
    public Collider2D colliderCharacter1, colliderCharacter2;
    public float jumpSpeed;
    public Sprite crouch, standCharacter, deathCharacter;
    public bool reverseGravity = false;
    public bool playerDeath;
    public GameObject GameOverMenu;
    public Animator animator;

    void Awake()
    {
        colliderCharacter1.enabled = true;
        colliderCharacter2.enabled = false;
    }
    void Start()
    {
        rigidCharacter = GetComponent<Rigidbody2D>();
        characterSprite = GetComponent<SpriteRenderer>();
        
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W) && Mathf.Abs(rigidCharacter.velocity.y)< 0.01f)
        {
            rigidCharacter.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);
            animator.SetBool("jumpTime", true);
            //characterSprite.sprite = standCharacter;
        }
       

        if (Input.GetKey(KeyCode.S))
        {
            colliderCharacter1.enabled = false;
            colliderCharacter2.enabled = true;
            animator.SetBool("crouchTime", true);
            //characterSprite.sprite = crouch;
        }
        else
        {
            colliderCharacter1.enabled = true;
            colliderCharacter2.enabled = false;
            animator.SetBool("crouchTime", false);
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Portal"))
        {
            RevertGravity();
        }
    }

    public void RevertGravity()
    {
        rigidCharacter.gravityScale *= -1;
        jumpSpeed *= -1;
        reverseGravity = !reverseGravity;
        characterSprite.flipY = reverseGravity;
    }

    public void Death()
    {
        GameOverMenu.SetActive(true);
        animator.enabled = false;
        characterSprite.sprite = deathCharacter;
        Time.timeScale = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerDestroyer"))
        {
    
            //Death();
           
        }

        if(collision.gameObject.CompareTag("Pincho"))
        {
     
            //Death();
        }

        if (collision.gameObject.CompareTag("Suelo"))
        {
            animator.SetBool("jumpTime",false);
        }
    }
}
