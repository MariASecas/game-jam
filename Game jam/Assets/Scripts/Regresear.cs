﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Regresear : MonoBehaviour
{
    public void Regresar()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        
    }
}
