﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background_Movement : MonoBehaviour
{

    public float bkSpeed;
    
    void Update()
    {
        transform.position -= new Vector3(bkSpeed * Time.deltaTime, 0,0);
    }
}
