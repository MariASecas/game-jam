﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pausa : MonoBehaviour
{
    public  bool gameP;
    public GameObject menuP;
   private void Start()
    {
        menuP.SetActive(false);
        //seguroP.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            btnPause();
        }
    }
        public void bntResume()   
    {
            menuP.SetActive(false);
            Time.timeScale = 1;
            gameP = false;
        }
        public void btnPause()
        {
            menuP.SetActive(true);
            Time.timeScale = 0;
            gameP = true;
        }     
    public void Exit()
    {
        Application.Quit();
    }
}

        